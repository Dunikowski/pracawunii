<?php 
require_once '../../../../../wp-load.php';
class JobOffersAjaxSearch
{

    private static  $offset = 0;
    private static $limit = 10;
    private static $country = '';
    private static $profession = '';
  
    public static function init($a = Null , $b = Null, $c = Null, $d = Null){

       

        if($a != Null) {
           self::$country = $a;
        }

        if($b != Null) {
            self::$limit = $b;
        }

        if($c != Null) {
            self::$offset = $c;
        }
        if($d != Null) {
            self::$profession = $d;
        }
    }
  
   public static function displayItems(){
    
    global $wpdb;

    $i = 'SELECT DATE_POSTED,TITLE,COUNTRY,CITY,COMPANY_NAME,JOB_URL FROM '.$wpdb->prefix.'job_offers  WHERE';

    if(self::$country != '' && self::$profession != ''){ 

        $i .= ' TITLE LIKE \'%'.self::$profession.'%\' AND COUNTRY like \''.self::$country.'\'';

    }elseif(self::$country != ''){
    
        $i .= ' COUNTRY like \''.self::$country.'\'';

    }else{

        $i .= ' TITLE LIKE \'%'.self::$profession.'%\''; 
    }
    
    if(self::$offset == 0) {
        $i .=' LIMIT '.$limit;
    } else {
        $i .=' LIMIT '.self::$offset.', '.self::$limit;
    }

    $offer_job = $wpdb->get_results($i,ARRAY_A);

       echo json_encode($offer_job);
   }
}

JobOffersAjaxSearch::init($_POST["country"],$_POST["limit"],$_POST["offset"],$_POST["profession"]);
JobOffersAjaxSearch::displayItems();

