(function($) {

  let PracaWUnii = {};
  PracaWUnii.body = $('body');
  PracaWUnii.widthWindow = $(window).width();

  if (PracaWUnii.body.length > 0 && PracaWUnii.body.hasClass('page-template-home-page')) {
    PracaWUnii.body.find('.js-main-carousel').slick({
      rtl: true,
      lazyLoad: 'ondemand',
      autoplay: true,
      speed: 1500,
      autoplaySpeed: 3000
    });
  }

let slideElement = {};
slideElement.init = function (el,ct){
  const ele = $(el);
  const cta = $(ct);

  cta.on('click',function(){

  const _this =$(this);

    if(ele.is(":hidden")){
      ele.slideDown();
      _this.removeClass('hide');
    }else {
      ele.slideUp();
      _this.addClass('hide');
    }
  });
}

slideElement.init('.js-sidebar-menu .widget_nav_menu > div:not(.widget-title)','.js-sidebar-menu .widget_nav_menu > .widget-title');
  let MobileMenu = {};
  MobileMenu.init = function() {
   

    const _nav = $('#main-nav');
    if (_nav.length > 0 && _nav.find("span.close").length == 0) {
      _nav.prepend("<span class='close'></span>");
    }
    const _span = _nav.find("span.close");
    if (_span.length > 0) {
      _span.on("click", function() {
        _nav.siblings('button.navbar-toggler').click();
      });
    }
  }
  MobileMenu.removeBTN = function () {
    const _nav = $('#main-nav');
    const _btn =_nav.find("span.close");
    if (_nav.length > 0 && _btn.length > 0) {
      _btn.remove();
    }
    return;
  }

  if (PracaWUnii.widthWindow < 1200) {
    MobileMenu.init();
  }
if(PracaWUnii.widthWindow < 600){
  $('.js-sidebar-menu .widget_nav_menu > .widget-title').click();
}
  $(window).on('resize', function() {
    PracaWUnii.widthWindow = $(window).width();
    

    if (PracaWUnii.widthWindow < 1200) {
      MobileMenu.init();
    } else {
      MobileMenu.removeBTN();
    }
  
  });
})(jQuery)