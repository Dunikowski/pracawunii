(function($){
    $(document).ready(function(){

        let JobOffersSearch = {};
        JobOffersSearch.container = $('.js-job-offers-search');
        JobOffersSearch.isLoad = false;
        JobOffersSearch.ajax = function(){
          
           const _country = JobOffersSearch.container.data("country") || '';
           const _offset = JobOffersSearch.container.find('.item').length;
           const _limit = JobOffersSearch.container.data("limit") || 20;
           const _profession = JobOffersSearch.container.data("profession") || '';
           const _loader = JobOffersSearch.container.find('>.loader');
           const _max_offers = JobOffersSearch.container.data("max-offers");
           if(_offset >=_max_offers ) return;
           _loader.addClass('show');
            $.ajax({
                url: myAjax.ajaxurl,
                type: 'post',
                data: {
                    country: _country,
                    offset: _offset,
                    limit: _limit,
                    profession: _profession
                },
                success: function( response ) {
                    let i = '';
                  
                    const _res = JSON.parse(response);
                    
                    _res.forEach(element => {
                        i += `<div class="item">
                            <div class="w-flag">
                                <span class="${element.COUNTRY.toLowerCase()}"></span>
                            </div>
                            <a href="${element.JOB_URL}" target="_blank">${element.TITLE} - ${element.COUNTRY}</a>
                            <p class="w-content-bottom">
                            ${element.COUNTRY} - ${element.CITY}, ${element.COMPANY_NAME},  ${element.DATE_POSTED.split(' ')[0]}
                            </p>
                            <a href="${element.JOB_URL}" class="cta-arrow" target="_blank">Szczegóły oferty</a>
                        </div>`;
                        
                   });
                  
                JobOffersSearch.container.append($(i));
                JobOffersSearch.isLoad = false;
                _loader.removeClass('show');
                },
            });
        }

        if(JobOffersSearch.container.length > 0){
            $(window).on('scroll', function() {
           
                if (!JobOffersSearch.isLoad && JobOffersSearch.container.offset().top + JobOffersSearch.container.outerHeight(true) <= $(window).scrollTop() +  $(window).height()) {
                    JobOffersSearch.isLoad = true;
                    JobOffersSearch.ajax();
                }
                return;
              });
        } 
    });
    
})(jQuery)