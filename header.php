<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */
$section_navbar_top = get_field('navbar_top',12);
$section_group_words = get_field('group_words',12)
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	
    <?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
        
	<header id="masthead" class="site-header navbar-static-top <?php echo wp_bootstrap_starter_bg_class(); ?>" role="banner">
        <div class="w-content">
            <nav class="navbar navbar-expand-xl p-0">
                <?php if($section_navbar_top):;?>
                    <div class="brand" >
                        <a href="<?php echo get_permalink($section_navbar_top['icon_hiperlink']);?>" rel="nofollow">
                            <img src="<?php echo $section_navbar_top['icon']['url'];?>" alt="<?php echo $section_navbar_top['icon']['alt'];?>">
                        </a>
                        <?php if($section_group_words):;?>
                        <div class="europa-jobs">
                            <?php foreach($section_group_words as $words_item):;?>
                            <span><?php echo $words_item['words'];?></span>
                            <?php endforeach;?>
                        </div>
                        <?php endif;?>
                    </div>        
                <?php endif;?>
               
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav" aria-controls="" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <?php
                wp_nav_menu(array(
                'theme_location'    => 'primary',
                'container'       => 'div',
                'container_id'    => 'main-nav',
                'container_class' => 'collapse navbar-collapse justify-content-end',
                'menu_id'         => false,
                'menu_class'      => 'navbar-nav',
                'depth'           => 3,
                'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
                'walker'          => new wp_bootstrap_navwalker()
                ));
                ?>

            </nav>
        </div>
	</header><!-- #masthead -->

<?php endif; ?>