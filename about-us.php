<?php
/**
* Template Name: O nas
 */
$section_icon_top_lop = get_field('icon_top_lop');
$section_banner_group = get_field('banner_group');
$section_loop_column_text = get_field('loop_column_text');
$group_more = get_field('group_more');
$section_brand_icon = get_field('brand_icon_loop');
get_header(); ?>

<section class="about-us">
<div class="w-top">
	<?php the_content();?>
	<?php if($section_icon_top_lop):;?>
	<div class="w-icon-top w-content">
	<?php foreach($section_icon_top_lop as $item_icon_top):;?>
		<img src="<?php echo $item_icon_top['icon']['url'];?>" alt="<?php echo $item_icon_top['icon']['alt'];?>">
	<?php endforeach;?>
	</div>
	<?php endif;?>
</div>

<?php if($section_banner_group ):;?>
<div class="w-banner bg-img">
	<?php if($section_banner_group['bg_img']):;?>
		<img src="<?php echo $section_banner_group['bg_img']['url'];?>" alt="<?php echo $section_banner_group['bg_img']['alt'];?>">
	<?php endif;?>
	<?php if($section_banner_group['text']):;?>
	<div class="w-text">
		<?php echo $section_banner_group['text'];?>
	</div>
	<?php endif;?>
</div>
<?php endif;?>
<?php if($section_loop_column_text):;?>
<div class="w-column-text w-content">
<?php foreach($section_loop_column_text as $item_text):;?>
	<div class="w-item-text">
		<?php echo $item_text['text'];?>
	</div>
<?php endforeach;?>
</div>
<?php endif;?>
<?php if($group_more ):;?>
<div class="w-more-information">
	<?php if($group_more['icon']):;?>
		<img src="<?php echo $group_more['icon']['url'];?>" alt="<?php echo $group_more['icon']['alt'];?>">
	<?php endif;?>
	<?php if($group_more['text']):;?>
	<div class="w-text">
		<?php echo $group_more['text'];?>
	</div>
	<?php endif;?>
</div>
<?php endif;?>
<?php if($section_brand_icon):;?>
	<div class="w-brand-icons">
	<?php foreach($section_brand_icon as $item_brand):;?>
		<img src="<?php echo $item_brand['icon']['url'];?>" alt="<?php echo $item_brand['icon']['alt'];?>">
	<?php endforeach;?>
	</div>
	<?php endif;?>
</section>

<?php
get_footer();
