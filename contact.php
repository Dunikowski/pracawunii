<?php
/**
* Template Name: Kontakt
 */

$group_left_column = get_field('group_left_column');
$group_right_column = get_field('group_right_column');
get_header(); 
if ( function_exists('yoast_breadcrumb') ) {
  yoast_breadcrumb( '<div id="breadcrumbs" class="breadcrumbs"><div class="w-content"','</div></div>' );
}	
?>

	<section  class="contact w-content primary">
 
  <h1 class="title"><?php the_title();?></h1>

      
	

		<div class="w-content-contact">
    <?php if($group_left_column  ):;?>
    <div class="w-left">
    <?php if($group_left_column['heading']  ):;?>
        <div class="heading uppercase"><?php echo $group_left_column['heading'];?></div>
      <?php endif;?>
      <?php if($group_left_column['loop']  ):;?>
        <?php foreach($group_left_column['loop'] as $item_left_column):;?>
          <div class="text"><?php echo $item_left_column['text'];?></div>
        <?php endforeach;?>
      <?php endif;?>
    </div>
    <?php endif;?>
  
    
    <?php if($group_right_column):;?>
    <div class="w-right">
      <?php if($group_right_column['heading']  ):;?>
          <div class="heading uppercase"><?php echo $group_right_column['heading'];?></div>
        <?php endif;?>
        <?php if($group_right_column['first_text_area']  ):;?>
          <div class="text"><?php echo $group_right_column['first_text_area'];?></div>
        <?php endif;?>
        <?php if($group_right_column['cta_first']['text']  ):;?>
          <a class="cta-gold" href="<?php echo $group_right_column['cta_first']['hiperlink'];?>" rel="follow" ><?php echo $group_right_column['cta_first']['text'];?></a>
        <?php endif;?>
        <?php if($group_right_column['second_text_area']  ):;?>
          <div class="text"><?php echo $group_right_column['second_text_area'];?></div>
        <?php endif;?>
        <?php if($group_right_column['cta_second']['text']  ):;?>
          <a class="cta-gold blue" href="<?php echo $group_right_column['cta_second']['hiperlink'];?>" rel="nofollow" target="_blank"><?php echo $group_right_column['cta_second']['text'];?></a>
        <?php endif;?>
    </div>
    <?php endif;?>
          
    </div><!-- end w-content-contact-->
   
	</section>

<?php
get_footer();
