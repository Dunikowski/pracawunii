<?php
/**
* Template Name: Kategoria artykułów
 */

get_header(); 
$select_articles = get_field('article_category');
    if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb( '<div id="breadcrumbs" class="breadcrumbs"><div class="w-content"','</div></div>' );
    }
    ;?>
<section class="content-listing-category">

  <div class="w-content">
  
    <div class="w-category-item">
      <h1 class="heading"><?php echo the_title();?></h1>
      <?php if(select_articles):;?>
      <div class="w-posts">
        <?php foreach(get_posts(array('category' => $select_articles)) as $item_cp): ;?>
        <a class="w-item" href="<?php echo get_permalink($item_cp->ID);?>">
          <?php  

					$section_post = get_field('bg_tiles',$item_cp->ID);

					if($section_post):

					;?>
          <div class="w-img">
          <div class="filter"></div>
            <img src="<?php echo $section_post['url'];?>" alt="<?php echo $section_post['alt'];?>">
          </div>
          <?php endif;?>
          <p class="post-title"><?php echo $item_cp->post_title ;?></p>
        </a>
        <?php endforeach;
        wp_reset_postdata();
        ?>
      </div>
          <?php endif;?>
    </div>
  </div>
</section>

<?php
get_footer();