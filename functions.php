<?php
/**
 * WP Bootstrap Starter functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WP_Bootstrap_Starter
 */

if ( ! function_exists( 'wp_bootstrap_starter_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function wp_bootstrap_starter_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on WP Bootstrap Starter, use a find and replace
	 * to change 'wp-bootstrap-starter' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'wp-bootstrap-starter', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'wp-bootstrap-starter' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'wp_bootstrap_starter_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

    function wp_boostrap_starter_add_editor_styles() {
        add_editor_style( 'custom-editor-style.css' );
    }
    add_action( 'admin_init', 'wp_boostrap_starter_add_editor_styles' );

}
endif;
add_action( 'after_setup_theme', 'wp_bootstrap_starter_setup' );


/**
 * Add Welcome message to dashboard
 */
function wp_bootstrap_starter_reminder(){
        $theme_page_url = 'https://afterimagedesigns.com/wp-bootstrap-starter/?dashboard=1';

            if(!get_option( 'triggered_welcomet')){
                $message = sprintf(__( 'Welcome to WP Bootstrap Starter Theme! Before diving in to your new theme, please visit the <a style="color: #fff; font-weight: bold;" href="%1$s" target="_blank">theme\'s</a> page for access to dozens of tips and in-depth tutorials.', 'wp-bootstrap-starter' ),
                    esc_url( $theme_page_url )
                );

                printf(
                    '<div class="notice is-dismissible" style="background-color: #6C2EB9; color: #fff; border-left: none;">
                        <p>%1$s</p>
                    </div>',
                    $message
                );
                add_option( 'triggered_welcomet', '1', '', 'yes' );
            }

}
add_action( 'admin_notices', 'wp_bootstrap_starter_reminder' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */

function wp_bootstrap_starter_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'wp_bootstrap_starter_content_width', 1170 );
}
add_action( 'after_setup_theme', 'wp_bootstrap_starter_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function wp_bootstrap_starter_widgets_init() {
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar', 'wp-bootstrap-starter' ),
        'id'            => 'sidebar-1',
        'description'   => esc_html__( 'Add widgets here.', 'wp-bootstrap-starter' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<div class="widget-title">',
        'after_title'   => '</div>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Footer 1', 'wp-bootstrap-starter' ),
        'id'            => 'footer-1',
        'description'   => esc_html__( 'Add widgets here.', 'wp-bootstrap-starter' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Footer 2', 'wp-bootstrap-starter' ),
        'id'            => 'footer-2',
        'description'   => esc_html__( 'Add widgets here.', 'wp-bootstrap-starter' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Footer 3', 'wp-bootstrap-starter' ),
        'id'            => 'footer-3',
        'description'   => esc_html__( 'Add widgets here.', 'wp-bootstrap-starter' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
}
add_action( 'widgets_init', 'wp_bootstrap_starter_widgets_init' );



/**
 * Enqueue scripts and styles.
 */
function wp_bootstrap_starter_scripts() {

    // wp_enqueue_style( 'wp-bootstrap-starter-bootstrap-css', get_template_directory_uri() . '/inc/assets/css/bootstrap.min.css' );
    
    wp_enqueue_style( 'praca-w-unii-css', get_template_directory_uri() . '/style.min.css' );
    wp_enqueue_style( 'wp-bootstrap-starter-style', get_stylesheet_uri() );
    if(get_theme_mod( 'theme_option_setting' ) && get_theme_mod( 'theme_option_setting' ) !== 'default') {
        wp_enqueue_style( 'wp-bootstrap-starter-'.get_theme_mod( 'theme_option_setting' ), get_template_directory_uri() . '/inc/assets/css/presets/theme-option/'.get_theme_mod( 'theme_option_setting' ).'.css', false, '' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'poppins-lora') {
        wp_enqueue_style( 'wp-bootstrap-starter-poppins-lora-font', 'https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i|Poppins:300,400,500,600,700' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'montserrat-merriweather') {
        wp_enqueue_style( 'wp-bootstrap-starter-montserrat-merriweather-font', 'https://fonts.googleapis.com/css?family=Merriweather:300,400,400i,700,900|Montserrat:300,400,400i,500,700,800' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'poppins-poppins') {
        wp_enqueue_style( 'wp-bootstrap-starter-poppins-font', 'https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'roboto-roboto') {
        wp_enqueue_style( 'wp-bootstrap-starter-roboto-font', 'https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'arbutusslab-opensans') {
        wp_enqueue_style( 'wp-bootstrap-starter-arbutusslab-opensans-font', 'https://fonts.googleapis.com/css?family=Arbutus+Slab|Open+Sans:300,300i,400,400i,600,600i,700,800' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'oswald-muli') {
        wp_enqueue_style( 'wp-bootstrap-starter-oswald-muli-font', 'https://fonts.googleapis.com/css?family=Muli:300,400,600,700,800|Oswald:300,400,500,600,700' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'montserrat-opensans') {
        wp_enqueue_style( 'wp-bootstrap-starter-montserrat-opensans-font', 'https://fonts.googleapis.com/css?family=Montserrat|Open+Sans:300,300i,400,400i,600,600i,700,800' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'robotoslab-roboto') {
        wp_enqueue_style( 'wp-bootstrap-starter-robotoslab-roboto', 'https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700|Roboto:300,300i,400,400i,500,700,700i' );
    }
    if(get_theme_mod( 'preset_style_setting' ) && get_theme_mod( 'preset_style_setting' ) !== 'default') {
        wp_enqueue_style( 'wp-bootstrap-starter-'.get_theme_mod( 'preset_style_setting' ), get_template_directory_uri() . '/inc/assets/css/presets/typography/'.get_theme_mod( 'preset_style_setting' ).'.css', false, '' );
    }
    //Color Scheme
    /*if(get_theme_mod( 'preset_color_scheme_setting' ) && get_theme_mod( 'preset_color_scheme_setting' ) !== 'default') {
        wp_enqueue_style( 'wp-bootstrap-starter-'.get_theme_mod( 'preset_color_scheme_setting' ), get_template_directory_uri() . '/inc/assets/css/presets/color-scheme/'.get_theme_mod( 'preset_color_scheme_setting' ).'.css', false, '' );
    }else {
        wp_enqueue_style( 'wp-bootstrap-starter-default', get_template_directory_uri() . '/inc/assets/css/presets/color-scheme/blue.css', false, '' );
    }*/

    wp_register_script( "offer-job-search-ajax", get_template_directory_uri() . '/inc/assets/js/job_offers_search.js', array('jquery'),false,true);

	wp_enqueue_script('jquery');

    // Internet Explorer HTML5 support
    wp_enqueue_script( 'html5hiv',get_template_directory_uri().'/inc/assets/js/html5.js', array(), '3.7.0', false );
    wp_script_add_data( 'html5hiv', 'conditional', 'lt IE 9' );

	// load bootstrap js
    wp_enqueue_script('wp-bootstrap-starter-popper', get_template_directory_uri() . '/inc/assets/js/popper.min.js', array(), '', true );
    if ( get_theme_mod( 'boostrap_cdn_setting' ) === 'yes' ) {
    	wp_enqueue_script('wp-bootstrap-starter-bootstrapjs', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array(), '', true );
    } else {
        wp_enqueue_script('wp-bootstrap-starter-bootstrapjs', get_template_directory_uri() . '/inc/assets/js/bootstrap.min.js', array(), '', true );
    }
    wp_enqueue_script('wp-bootstrap-starter-themejs', get_template_directory_uri() . '/inc/assets/js/theme-script.min.js', array(), '', true );
	wp_enqueue_script( 'wp-bootstrap-starter-skip-link-focus-fix', get_template_directory_uri() . '/inc/assets/js/skip-link-focus-fix.min.js', array(), '20151215', true );
    wp_enqueue_script('wp-my-scripts', get_template_directory_uri() . '/inc/assets/js/all.min.js', array(), '', true );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
    }
    
   
    wp_localize_script( 'offer-job-search-ajax', 'myAjax', array( 'ajaxurl' => get_template_directory_uri() . '/inc/assets/job_offers_search_ajax.php')); 
    wp_enqueue_script( 'offer-job-search-ajax' ); 
}
add_action( 'wp_enqueue_scripts', 'wp_bootstrap_starter_scripts' );


function wp_bootstrap_starter_password_form() {
    global $post;
    $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
    $o = '<form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" method="post">
    <div class="d-block mb-3">' . __( "To view this protected post, enter the password below:", "wp-bootstrap-starter" ) . '</div>
    <div class="form-group form-inline"><label for="' . $label . '" class="mr-2">' . __( "Password:", "wp-bootstrap-starter" ) . ' </label><input name="post_password" id="' . $label . '" type="password" size="20" maxlength="20" class="form-control mr-2" /> <input type="submit" name="Submit" value="' . esc_attr__( "Submit", "wp-bootstrap-starter" ) . '" class="btn btn-primary"/></div>
    </form>';
    return $o;
}
add_filter( 'the_password_form', 'wp_bootstrap_starter_password_form' );



/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load plugin compatibility file.
 */
require get_template_directory() . '/inc/plugin-compatibility/plugin-compatibility.php';

/**
 * Load custom WordPress nav walker.
 */
if ( ! class_exists( 'wp_bootstrap_navwalker' )) {
    require_once(get_template_directory() . '/inc/wp_bootstrap_navwalker.php');
}

function initOptionSearchOffersJobArgs(){

    if(!get_option( 'SearchOffersJobArgs-45698214', $default = false )){
        $y = serialize(array(
            'country' => '',
            'profession' => ''
        ));

        add_option('SearchOffersJobArgs-45698214',$y);
    }
}

add_action('init', 'initOptionSearchOffersJobArgs');

function searchOffersJobResuts(){

    if(get_option( 'SearchOffersJobArgs-45698214', $default = false )){

        $y = unserialize(get_option( 'SearchOffersJobArgs-45698214'));
    }

    $profession = $y['profession'];
    $country = $y['country'];

    global $wpdb;

    $i = 'SELECT DATE_POSTED,TITLE,COUNTRY,CITY,COMPANY_NAME,JOB_URL FROM '.$wpdb->prefix.'job_offers  WHERE';
    $ic = 'SELECT COUNT(id) FROM '.$wpdb->prefix.'job_offers  WHERE';
    if($country != '' && $profession != ''){ 

        $i .= ' TITLE LIKE \'%'.$profession.'%\' AND COUNTRY like \''.$country.'\'';
        $ic .= ' TITLE LIKE \'%'.$profession.'%\' AND COUNTRY like \''.$country.'\'';
    }elseif($country != ''){
    
        $i .= ' COUNTRY like \''.$country.'\'';
        $ic .= ' COUNTRY like \''.$country.'\'';
    }else{

        $i .= ' TITLE LIKE \'%'.$profession.'%\''; 
        $ic .= ' TITLE LIKE \'%'.$profession.'%\''; 
    }
    
    $i .= ' LIMIT 20';

    $offer_job = $wpdb->get_results($i,ARRAY_A);
    $offer_job_count = $wpdb->get_var($ic);;
    $items = '';

    if(count($offer_job) > 0){
        $items .= '<div class="loader"><img src="'.get_template_directory_uri().'/image/giphy.gif"></div>';
      
        foreach($offer_job as $item_job){
        
            $items .= '<div class="item">';
            $items .= '<div class="w-flag">';
            $items .=  '<span class="'.strtolower($item_job['COUNTRY']).'"></span>';
            $items .= '</div>';
            $items .=  '<a href="'.$item_job['JOB_URL'].'" target="_blank">'.$item_job['TITLE'].' - '.$item_job['COUNTRY'].'</a>';
            $items .= '<p class="w-content-bottom">';
            $items .=  $item_job['COUNTRY'].' - '.$item_job['CITY'].', '.$item_job['COMPANY_NAME'].', '.explode(" ",$item_job['DATE_POSTED'])[0];
            $items .= '</p>';
            $items .=  '<a href="'.$item_job['JOB_URL'].'" class="cta-arrow" target="_blank">Szczegóły oferty</a>';
            $items .=  '</div>';
        }
        $j = '<div class="w-job-offers js-job-offers-search" data-max-offers="'.$offer_job_count.'"';

        if($country != '' && $profession != ''){
    
            $j .= ' data-country="'.$country.'" data-profession="'. $profession .'">'.$items.'</div>';
    
        }elseif($country != ''){
    
            $j .= ' data-country="'.$country.'">'.$items.'</div>';
    
        }else{
    
            $j .= ' data-profession="'. $profession .'">'.$items.'</div>'; 
    
        }
    
        echo  $j;
    } else {
        
            if($country != '' && $profession != ''){
                $items .='<div class="no-found">Przepraszamy, ale nic nie pasowało do wyszukiwanych słów kluczowych.';
                $items .= '<p>Stanowisko: '.$profession.'</p>';
                $items .= '<p>Kraj: '.$country.'</p>';
                
            }elseif($country != ''){
                $items .='<div class="no-found">Przepraszamy, ale nic nie pasowało do wyszukiwanego słowa kluczowego.';
                $items .= '<p>Kraj: '.$country.'</p>';
            }else{
                $items .='<div class="no-found">Przepraszamy, ale nic nie pasowało do wyszukiwanego słowa kluczowego.';
                $items .= '<p>Stanowisko: '.$profession.'</p>';
            }
            
            $items .=' Spróbuj ponownie, używając innych słów kluczowych:</div>';
            echo $items;
        }
    }

add_action('search_offers_job_resuts', 'searchOffersJobResuts', 10);

function searchOferssJob(){

    $country;
    $profession;

    if(isset($_POST['country'])){

        $country = $_POST['country'];

    } else{

        $country = null;

    }

    if(isset($_POST['profession'])){

        $profession = $_POST['profession'];

    }else{

        $profession = null;

    }

    if(class_exists('JobOffers')){
        $s_id = get_field('page_results',12);
        if($country != null && $profession != null){
            if(get_option( 'SearchOffersJobArgs-45698214', $default = false )){

                $y = serialize(array(
                    'country' => $country,
                    'profession' => $profession
                ));

                update_option('SearchOffersJobArgs-45698214',$y);

            }

             exit( wp_redirect(get_permalink($s_id)));
            
        }elseif($profession != null){

            if(get_option( 'SearchOffersJobArgs-45698214', $default = false )){

                $y = serialize(array(
                    'country' => '',
                    'profession' => $profession
                ));

                update_option('SearchOffersJobArgs-45698214',$y);
            }

             exit( wp_redirect(get_permalink($s_id)));

        }elseif($country != null) {

            if(get_option( 'SearchOffersJobArgs-45698214', $default = false )){

                $y = serialize(array(
                    'country' => $country,
                    'profession' => ''
        
                ));

                update_option('SearchOffersJobArgs-45698214',$y);
            }

             exit( wp_redirect(get_permalink($s_id)));

        } else {
       
            exit( wp_redirect(get_permalink(get_field('page_results_all',12))));

        }

    }else {

        exit();
    }

}
add_action('admin_post_search_offers_job','searchOferssJob');
add_action('admin_post_nopriv_search_offers_job','searchOferssJob');

function workInUnionOffersWeek(){
    if(class_exists('JobOffers')){
        $loop = get_field('tiles_week_offers',12);
        $announces = count($loop);
        $j = '';
        if($announces){
        
            global $wpdb;
            $i = 'SELECT DATE_POSTED,TITLE,COMPANY_NAME,JOB_URL FROM '.$wpdb->prefix.'job_offers ORDER BY RAND() LIMIT '.$announces;
            $resuts = $wpdb->get_results($i,ARRAY_A);

            for($k = 0; $k < $announces; $k++){
                $j .= '<div class="item bg-img post-tiles">';
                $j .= '<img src="'.$loop[$k]['bg_img']['url'].'" alt="'.$loop[$k]['bg_img']['alt'].'">';
                $j .= '<div class="bg-wrap">';
                $j .= '<p class="title">Oferta tygodnia<p>';
                $j .= '<a class="heading" href="'.$resuts[$k]['JOB_URL'].'" target="_blank">'.$resuts[$k]['TITLE'].'</a>';
                $j .= '<p class="heading-smoll">'.$resuts[$k]['COMPANY_NAME'].' / '.explode(" ",$resuts[$k]['DATE_POSTED'])[0].'</p>';
                $j .= '</div>';
                $j .= '</div>';
            } 
        } else {
            $j = 'W polu najnowsze ogłoszenia można wybrac do 5 ogłoszeń';
        }
    
        echo $j;
    }
}

add_action('offers_week', 'workInUnionOffersWeek');

function workInUnionLatestPostCategory(){
    
    $i = '';
    $tiles_5 = get_field('tiles_5',12);
    if($tiles_5['category']){
        $args = array(
            'post_type' => 'post',
            'posts_per_page' => 1,
            'cat' => $tiles_5['category'],
            'orderby' => 'date',
            'order'   => 'DESC',
            );
            $the_query = new WP_Query( $args );
          if ( $the_query->have_posts() ) {
    
            $i .= '<img src="'.$tiles_5['tlo_kafelka']['url'].'" alt="'.$tiles_5['tlo_kafelka']['alt'].'">';
            $i .= '<a class="heding bg-wrap" href="'.$the_query->posts[0]->guid.'">'.$the_query->posts[0]->post_title.'</a>';
        
        } else {
               $i = 'Nie znaleziono postu o id kategorii'.$tiles_5['category'];
        }
    
          wp_reset_postdata();
    } else {
        $i = 'Nie wybrano na stronie Home kategorii kafekla 5';
    }
   
    echo $i;
}
add_action('latest_post_category', 'workInUnionLatestPostCategory');

function workInUnionLatestOffersJob(){
    if(class_exists('JobOffers')){
        $announces = get_field('leatest-announces-job',12);

        $j = '';
        if($announces['count_announces'] < 6){
        
            global $wpdb;
            $i = 'SELECT DATE_POSTED,TITLE,COMPANY_NAME,JOB_URL FROM '.$wpdb->prefix.'job_offers LIMIT '.$announces['count_announces'];
            $resuts = $wpdb->get_results($i,ARRAY_A);

            foreach($resuts  as $item){
                $j .= '<div class="item">';
                $j .= '<a class="heading" href="'.$item['JOB_URL'].'" target="_blank">'.$item['TITLE'].'</a>';
                $j .= '<p class="heading-smoll">'.$item['COMPANY_NAME'].' / '.explode(" ",$item['DATE_POSTED'])[0].'</p>';
                $j .= '<a class="cta-arrow" href="'.$item['JOB_URL'].'" target="_blank"></a>';
                $j .= '</div>';
            } 
        } else {
            $j = 'W polu najnowsze ogłoszenia można wybrac do 5 ogłoszeń';
        }
        
        echo $j;
    }
}


add_action('latest_offers_job', 'workInUnionLatestOffersJob');

//Coment form
add_filter( 'comment_form_fields', 'mo_comment_fields_custom_order' );
function mo_comment_fields_custom_order( $fields ) {

	$author_field = $fields['author'];
	$email_field = $fields['email'];
	$url_field = $fields['url'];

	unset( $fields['author'] );
	unset( $fields['email'] );
	unset( $fields['url'] );
	// the order of fields is the order below, change it as needed:

	$fields['author'] = '<p class="comment-form-author"><input id="author" name="author" type="text" placeholder="Nazwa" value="" size="30" maxlength="245" required="required"></p>';
	$fields['email'] = '<p class="comment-form-email"><input id="email" name="email" type="email" placeholder="E-mail" value="" size="30" maxlength="100" aria-describedby="email-notes" required="required"></p>';
	$fields['url'] = '<p class="comment-form-url"><input id="url" name="url" type="url" placeholder="Witryna internetowa" value="" size="30" maxlength="200"></p>';
	// done ordering, now return the fields:
	return $fields;
}