<?php
/**
 * Template Name: Home
 */
$section_left_tiles = get_field('left_tiles');
$section_right_tiles = get_field('right_tiles');
$section_group_words = get_field('group_words',12);
$section_tiles_2 = get_field('tiles_2');
$section_tiles_3  = get_field('tiles_3');
$section_tiles_4 = get_field('tiles_4');
$section_tiles_6 = get_field('tiles_6');
$section_leatest_announces  = get_field('leatest-announces-job');
$section_tiles_8 = get_field('tiles_8');
$section_tiles_9 = get_field('tiles_9');
$section_tiles_10 = get_field('tiles_10');
$section_tiles_11 = get_field('tiles_11');
$section_tiles_12 = get_field('tiles_12');
$section_tiles_13 = get_field('tiles_13');
$section_seo_text = get_field('seo_text');
$section_form = get_field('form');
get_header();
?>
<div class="top-tiles">
  <?php if($section_left_tiles):;?>
  <div class="left-tiles bg-img">
    <?php if($section_left_tiles['bg_img']):;?>
    <img src="<?php echo $section_left_tiles['bg_img']['url'];?>" alt="<?php echo $section_left_tiles['bg_img']['alt'];?>">

    <?php endif;?>
    <?php if($section_left_tiles['icon']):;?>
    <a class="brand" href="<?php echo $section_left_tiles['hiperlink'];?>" rel="nofollow" target="_blank">
      <img src="<?php echo $section_left_tiles['icon']['url'];?>" alt="<?php echo $section_left_tiles['icon']['alt'];?>">
      <span class="cta-arrow">
        <?php if($section_group_words):;?>
        <div class="europa-jobs">
          <?php foreach($section_group_words as $words_item):;?>
          <span><?php echo $words_item['words'];?></span>
          <?php endforeach;?>
        </div>
        <?php endif;?>
      </span>
    </a>
    <?php endif;?>
  </div>
  <?php endif;?>

  <?php if($section_right_tiles):;?>
  <div class="right-tiles bg-img">
    <?php if($section_left_tiles['bg_img']):;?>
    <img src="<?php echo $section_right_tiles['bg_img']['url'];?>" alt="<?php echo $section_right_tiles['bg_img']['alt'];?>">
    <?php endif;?>
    <div class="w-content_tiles">
      <?php if($section_right_tiles['text_1']):;?>
      <div class="heading">
        <?php echo $section_right_tiles['text_1'];?>
      </div>
      <?php endif;?>
      <form action="<?php echo esc_url(admin_url('admin-post.php') ); ?>" class="job-offrs" method="post">
        <div class="w-inputs">
          <input type="text" name="profession" placeholder="<?php echo $section_form['text_placeholder_1'];?>">
          <input type="text" name="country" placeholder="<?php echo $section_form['text_placeholder_2'];?>">
          <input type="hidden" name="action" value="search_offers_job">
        </div>
       
        <input type="submit" class="cta-gold" value="<?php echo $section_form['cta_text'];?>">
      </form>
    </div>
  </div>
  <?php endif;?>
  <?php if($section_right_tiles):;?>
  <div class="right-tiles"></div>
  <?php endif;?>
</div>
<div class="w-content">
  <div class="tiles-center-top tiles">
    <div class="offers-week js-main-carousel" dir="rtl">
      <?php do_action('offers_week');?>
    </div>
    <div class="looking-job bg-img">
      <?php if($section_tiles_2['tlo']):;?>
      <img src="<?php echo $section_tiles_2['tlo']['url'];?>" alt="<?php echo $section_tiles_2['tlo']['alt'];?>">

      <?php endif;?>
      <div class="w-c-middle-tiles">
        <?php if( $section_tiles_2['title']):;?>
        <p class="heading"><?php echo $section_tiles_2['title'];?></p>
        <?php endif;?>
        <?php if( $section_tiles_2['text_1']):;?>
        <p class="text"><?php echo $section_tiles_2['text_1'];?></p>
        <?php endif;?>
        <?php if( $section_tiles_2['text_2']):;?>
        <a class="cta-arrow" href="<?php echo $section_tiles_2['hiperlink'];?>" target="_blank"><?php echo $section_tiles_2['text_2'];?></a>
        <?php endif;?>
      </div>
    </div>

    <div class="without-language bg-img">
      <?php if($section_tiles_3['tlo']):;?>
      <img src="<?php echo $section_tiles_3['tlo']['url'];?>" alt="<?php echo $section_tiles_3['tlo']['alt'];?>">

      <?php endif;?>

      <div class="w-c-middle-tiles">
        <?php if( $section_tiles_3['text']):;?>
        <a class="heading" href="<?php echo get_permalink($section_tiles_3['hiperlink']);?>"><?php echo $section_tiles_3['text'];?></a>
        <?php endif;?>
      </div>

    </div>
    <div class="apply-faster bg-img">
      <?php if($section_tiles_4['tlo']):;?>
      <img src="<?php echo $section_tiles_4['tlo']['url'];?>" alt="<?php echo $section_tiles_4['tlo']['alt'];?>">

      <?php endif;?>
      <div class="w-c-middle-tiles">
        <?php if( $section_tiles_4['title']):;?>
        <p class="heading"><?php echo $section_tiles_4['title'];?></p>
        <?php endif;?>

        <?php if( $section_tiles_4['text_2']):;?>
        <a class="cta-arrow" href="<?php echo $section_tiles_4['hiperlink'];?>" target="_blank"><?php echo $section_tiles_4['text_2'];?></a>
        <?php endif;?>
      </div>

    </div>
    <div class="post-category-1 bg-img">
      <?php do_action('latest_post_category');?>
    </div>
    <div class="check-offers bg-img">
      <?php if($section_tiles_6['tlo']):;?>
      <img src="<?php echo $section_tiles_6['tlo']['url'];?>" alt="<?php echo $section_tiles_6['tlo']['alt'];?>">
      <?php endif;?>
      <div class="w-c-middle-tiles">
        <?php if( $section_tiles_6['text']):;?>
        <div class="heading"><?php echo $section_tiles_6['text'];?></div>
        <?php endif;?>
      </div>
    </div>
  </div>
  <div class="tiles-center-middle">
    <div class="leatest-announces-job">
      <?php if($section_leatest_announces['title']):;?>
      <p class="heading"><?php echo $section_leatest_announces['title'];?></p>
      <?php endif;?>
      <?php if($section_leatest_announces['count_announces']):;?>
      <?php do_action('latest_offers_job');?>
      <?php endif;?>
    </div>
    <div class="right-posts">
      <?php if( $section_tiles_8['chose_post']):;?>
      <div class="top bg-img post-tiles">
        <?php if($section_tiles_8['bg_img']):;?>
        <img src="<?php echo $section_tiles_8['bg_img']['url'];?>" alt="<?php echo $section_tiles_8['bg_img']['alt'];?>">
        <?php endif;?>
        <a class="heding bg-wrap" href="<?php echo get_permalink($section_tiles_8['chose_post']);?>"><?php echo get_the_title($section_tiles_8['chose_post']);?></a>
      </div>
      <?php endif;?>
      <?php if( $section_tiles_9['chose_post']):;?>
      <div class="bottom bg-img post-tiles">
        <?php if($section_tiles_9['bg_img']):;?>
        <img src="<?php echo $section_tiles_9['bg_img']['url'];?>" alt="<?php echo $section_tiles_9['bg_img']['alt'];?>">
        <?php endif;?>
        <a class="heding bg-wrap" href="<?php echo get_permalink($section_tiles_9['chose_post']);?>"><?php echo get_the_title($section_tiles_9['chose_post']);?></a>
      </div>
      <?php endif;?>
    </div>
  </div>
  <div class="tiles-center-bottom">
    <div class="bg-img post-tiles">
      <?php if( $section_tiles_10['chose_post']):;?>
      <?php if($section_tiles_10['bg_img']):;?>
      <img src="<?php echo $section_tiles_10['bg_img']['url'];?>" alt="<?php echo $section_tiles_10['bg_img']['alt'];?>">
      <?php endif;?>
      <a class="heding bg-wrap" href="<?php echo get_permalink($section_tiles_10['chose_post']);?>"><?php echo get_the_title($section_tiles_10['chose_post']);?></a>
      <?php endif;?>
    </div>

    <div class="violet">
      <?php if( $section_tiles_11['text']):;?>
      <div class="w-c-middle-tiles">
        <a class="heading" href="<?php echo get_permalink($section_tiles_11['hiperlink']);?>"><?php echo $section_tiles_11['text'];?></a>
      </div>
      <?php endif;?>
    </div>
    <div class="yellow">
      <?php if( $section_tiles_12['title']):;?>
      <div class="w-c-middle-tiles">
        <div class="heading"><?php echo $section_tiles_12['title'];?></div>
        <a class="cta-arrow" href="<?php echo $section_tiles_12['hiperlink'];?>"><?php echo $section_tiles_12['text_2'];?></a>
      </div>
      <?php endif;?>
    </div>
    <div class="fb-blue bg-img">
      <?php if($section_tiles_13['bg_img']):;?>
      <img src="<?php echo $section_tiles_13['bg_img']['url'];?>" alt="<?php echo $section_tiles_13['bg_img']['alt'];?>">
      <?php endif;?>
      <?php if( $section_tiles_13['title']):;?>
      <div class="w-c-middle-tiles">
        <a class="heading" href="<?php echo $section_tiles_13['hiperlink'];?>" target="_blank"><?php echo $section_tiles_13['title'];?></a>
      </div>
      <?php endif;?>
    </div>
  </div>
  <?php  if($section_seo_text):;?>
  <div class="seo-text">
    <?php echo $section_seo_text;?>
  </div>
  <?php  endif;?>
  <?php
get_footer();