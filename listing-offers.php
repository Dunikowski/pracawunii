<?php
/**
 * Template Name: Listing offers
 */
$section_form = get_field('form',12);
$shortcode = get_field('shortcode');
get_header();
?>

  <?php 
    if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb( '<div id="breadcrumbs" class="breadcrumbs"><div class="w-content"','</div></div>' );
    }
    ;?>
<section class="content-listing-offers">
  <div class="w-content">
    <div class="head">
      <h1 class="title"><?php the_title();?></h1>
      <div class="text">
        <?php echo get_field('text');?>
      </div>
    </div>
    <aside class="sidebar-left js-sidebar-menu">
      <form action="<?php echo esc_url(admin_url('admin-post.php')); ?>" class="list-offrs" method="post">
          <input type="text" name="profession" placeholder="<?php echo $section_form['text_placeholder_1'];?>">
          <input type="text" name="country" placeholder="<?php echo $section_form['text_placeholder_2'];?>">
          <input type="hidden" name="action" value="search_offers_job">
        <input type="submit" class="cta-gold" value="<?php echo $section_form['cta_text'];?>">
      </form>
      <?php get_sidebar();?>
    </aside><!-- #secondary -->
    <div class="w-list">
      <?php 
      if($shortcode) {
        echo do_shortcode($shortcode);
      } else {
        
        do_action('search_offers_job_resuts');
   
      }
      ?>
    </div>
  </div>
</section>


<?php
get_footer();