<?php
/**
* Template Name: Porady i aktualności
 */

get_header(); 
$select_articles = get_field('article_category_loop');
    if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb( '<div id="breadcrumbs" class="breadcrumbs"><div class="w-content"','</div></div>' );
    }
    ;?>
<section class="content-listing-category">

  <div class="w-content">
    <?php foreach($select_articles as $item_c): ?>
    <div class="w-category-item">
      <a class="heading" href="<?php echo get_permalink($item_c['hiperlink']);?>"><?php echo get_cat_name($item_c['select_articles']);?></a>
      <div class="w-posts">
        <?php foreach(get_posts(array('numberposts' => '8','category' => $item_c['select_articles'])) as $item_cp): ;?>
        <a class="w-item" href="<?php echo get_permalink($item_cp->ID);?>">
          <?php  

					$section_post = get_field('bg_tiles',$item_cp->ID);

					if($section_post):

					;?>
          <div class="w-img">
            <div class="filter"></div>
            <img src="<?php echo $section_post['url'];?>" alt="<?php echo $section_post['alt'];?>">
          </div>

          <?php endif;?>

          <p class="post-title"><?php echo $item_cp->post_title ;?></p>
        </a>
          <?php endforeach;
        wp_reset_postdata()
        ;?>
      </div>
      <div class="w-cta-gold">
        <?php 
		$cta_gold = $item_c['text'];
		if($cta_gold):
		;?>
        <a class="cta-gold" href="<?php echo get_permalink($item_c['hiperlink']);?>" rel="nofollow">
          <?php echo $cta_gold;?>
        </a>
        <?php endif;?>
      </div>

    </div>
    <?php endforeach;?>
  </div>

</section>

<?php
get_footer();