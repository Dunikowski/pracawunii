<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WP_Bootstrap_Starter
 */
$section_form = get_field('form',12);
$content_text = get_field('text');
$content_cta_group = get_field('cta_group');
$section_recommended_for_you = get_field('post_heading',12);
$group_social_media = get_field('social_media',12);
get_header(); 
if ( function_exists('yoast_breadcrumb') ) {
	yoast_breadcrumb( '<div id="breadcrumbs" class="breadcrumbs"><div class="w-content"','</div></div>' );
  }	
?>
<div class="w-post">
  <section class="post">
    <h1 class="title"><?php the_title();?></h1>
    <p class="date"><?php echo get_the_date();?></p>
    <div class="text"><?php the_content();?></div>
    <?php if(has_post_thumbnail($post->ID) ):;?>
    <div class="bg-img thumnbail">
      <img src="<?php echo get_the_post_thumbnail_url($post->ID);?>">
    </div>
    <?php endif;?>

    <?php if($content_text):;?>
    <div class="text"><?php echo $content_text;?></div>
    <?php endif;?>
    <?php if($content_cta_group['text']):;?>
    <a class="cta-gold" href="<?php echo $content_cta_group['hiperlink'];?>"><?php echo $content_cta_group['text'];?></a>
    <?php endif;?>

    <div class="social-media">
      <?php if($group_social_media['label']):;?>
        <?php echo $group_social_media['label'];?>
      <?php endif;?>
      <?php if($group_social_media['facebook']):;?>
        <!-- Sharingbutton Facebook -->
        <a class="resp-sharing-button__link" href="https://facebook.com/sharer/sharer.php?u=https:<?php echo get_permalink();?>" target="_blank" rel="noopener" aria-label="">
        <div class="resp-sharing-button resp-sharing-button--facebook resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.77 7.46H14.5v-1.9c0-.9.6-1.1 1-1.1h3V.5h-4.33C10.24.5 9.5 3.44 9.5 5.32v2.15h-3v4h3v12h5v-12h3.85l.42-4z"/></svg>
          </div>
        </div>
      </a>
      <?php endif;?>
      <?php if($group_social_media['twitter']):;?>
        <!-- Sharingbutton Twitter -->
        <a class="resp-sharing-button__link" href="https://twitter.com/intent/tweet/?text=<?php the_title();?>&amp;url=https:<?php echo get_permalink();?>" target="_blank" rel="noopener" aria-label="">
          <div class="resp-sharing-button resp-sharing-button--twitter resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M23.44 4.83c-.8.37-1.5.38-2.22.02.93-.56.98-.96 1.32-2.02-.88.52-1.86.9-2.9 1.1-.82-.88-2-1.43-3.3-1.43-2.5 0-4.55 2.04-4.55 4.54 0 .36.03.7.1 1.04-3.77-.2-7.12-2-9.36-4.75-.4.67-.6 1.45-.6 2.3 0 1.56.8 2.95 2 3.77-.74-.03-1.44-.23-2.05-.57v.06c0 2.2 1.56 4.03 3.64 4.44-.67.2-1.37.2-2.06.08.58 1.8 2.26 3.12 4.25 3.16C5.78 18.1 3.37 18.74 1 18.46c2 1.3 4.4 2.04 6.97 2.04 8.35 0 12.92-6.92 12.92-12.93 0-.2 0-.4-.02-.6.9-.63 1.96-1.22 2.56-2.14z"/></svg>
            </div>
          </div>
        </a>
        <?php endif;?>
        <?php if($group_social_media['e_mail']):;?>
        <!-- Sharingbutton E-Mail -->
        <a class="resp-sharing-button__link" href="mailto:<?php echo $group_social_media['e_mail'];?>?subject=<?php the_title();?>" target="_self" rel="noopener" aria-label="" title="Wyślij na adres e-mail">
          <div class="resp-sharing-button resp-sharing-button--email resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M22 4H2C.9 4 0 4.9 0 6v12c0 1.1.9 2 2 2h20c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zM7.25 14.43l-3.5 2c-.08.05-.17.07-.25.07-.17 0-.34-.1-.43-.25-.14-.24-.06-.55.18-.68l3.5-2c.24-.14.55-.06.68.18.14.24.06.55-.18.68zm4.75.07c-.1 0-.2-.03-.27-.08l-8.5-5.5c-.23-.15-.3-.46-.15-.7.15-.22.46-.3.7-.14L12 13.4l8.23-5.32c.23-.15.54-.08.7.15.14.23.07.54-.16.7l-8.5 5.5c-.08.04-.17.07-.27.07zm8.93 1.75c-.1.16-.26.25-.43.25-.08 0-.17-.02-.25-.07l-3.5-2c-.24-.13-.32-.44-.18-.68s.44-.32.68-.18l3.5 2c.24.13.32.44.18.68z"/></svg>
            </div>
          </div>
        </a>
        <?php endif;?>
    </div>

    <?php
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
		?>
		<?php if($section_recommended_for_you):;?>
			<p class="heading"><?php echo $section_recommended_for_you;?></p>
		<?php endif;?>
    <div class="w-posts">
      <?php 
	
		foreach(get_posts(array('category' => wp_get_post_categories($post->ID), 'numberposts' => 8,'post__not_in'   => array( $post->ID ))) as $item_cp): 
		
		;?>
      <a class="w-item" href="<?php echo get_permalink($item_cp->ID);?>">
        <?php  
					$section_post = get_field('bg_tiles',$item_cp->ID);

					if($section_post):;?>
        <div class="w-img">
          <div class="filter"></div>
          <img src="<?php echo $section_post['url'];?>" alt="<?php echo $section_post['alt'];?>">
        </div>
        <?php endif;?>
        <p class="post-title"><?php echo $item_cp->post_title ;?></p>
      </a>
      <?php endforeach;
		wp_reset_postdata();
		?>
    </div>

  </section>
  <aside class="sidebar-right js-sidebar-menu">
  <div class="head">
      <p class="title">Oferty pracy</p>
      <div class="text">
        <?php echo get_field('text',182);?>
      </div>
    </div>
    <form action="<?php echo esc_url(admin_url('admin-post.php')); ?>" class="list-offrs" method="post">
      <input type="text" name="profession" placeholder="<?php echo $section_form['text_placeholder_1'];?>">
      <input type="text" name="country" placeholder="<?php echo $section_form['text_placeholder_2'];?>">
      <input type="hidden" name="action" value="search_offers_job">
      <input type="submit" class="cta-gold" value="<?php echo $section_form['cta_text'];?>">
    </form>
    <?php get_sidebar();?>
  </aside>
</div>
<?php

get_footer();